import { Component, Input, OnInit } from '@angular/core';
import { Note } from 'src/app/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input() notes: Note[];

  constructor(
    private router: Router,
    ) { }

  ngOnInit(): void {
  }

  viewNote(note: Note) {
    this.router.navigateByUrl(`note?id=${note.id}`)
  }
}
